import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { HomeTitleComponent } from './home-component/home-title/home-title.component';
import { HomeMainComponent } from './home-component/home-main/home-main.component';


@NgModule({
  declarations: [HomeComponent, HomeTitleComponent, HomeMainComponent],
  imports: [
    CommonModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
