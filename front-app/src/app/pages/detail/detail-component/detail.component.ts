import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute} from '@angular/router';

import { MakeupService } from './../../../services/makeup.service';
import { MakeupApiDetail } from 'src/app/models/models-interface';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
public detailcard: MakeupApiDetail[];
private cardId: string;
  constructor(private makeupService: MakeupService, private location: Location, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): void {
    this.route.paramMap.subscribe(params => {
      this.cardId = params.get('id');
    });
    this.makeupService.getDetail(Number(this.cardId)).subscribe((data: MakeupApiDetail[]) => {
      this.detailcard = data;
    }, (err) => {console.error(err.message);
    }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
