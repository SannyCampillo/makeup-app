import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HaulListComponent } from './haul-list-component/haul-list.component';

const routes: Routes = [{ path: '', component: HaulListComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HaulListRoutingModule { }
