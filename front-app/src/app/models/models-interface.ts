export interface MakeupApiList
  {
    id: string;
    brand: string;
    name: string;
    image_link: string;
  }

export interface MakeupApiDetail
 {
    id: string;
    brand: string;
    name: string;
    image_link: string;
    description: string;
    product_type: string;
    product_colors: Color;
 }

export interface Color
{
  hexvalue?: string;
  colorname: string;

}
