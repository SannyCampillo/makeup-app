import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

import { MakeupService } from './../../../services/makeup.service';
import { MakeupApiList } from 'src/app/models/models-interface';


@Component({
  selector: 'app-haul-list',
  templateUrl: './haul-list.component.html',
  styleUrls: ['./haul-list.component.scss']
})
export class HaulListComponent implements OnInit {

 public cardList: MakeupApiList[];

  constructor(private makeupService: MakeupService, private location: Location) {}

  ngOnInit(): void {
    this.getCardList();
  }

  public getCardList(): void {
    this.makeupService.getList().subscribe(
      (data: MakeupApiList[]) => {
        this.cardList = data;
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

  public goBack(): void {
    this.location.back();
  }
}
