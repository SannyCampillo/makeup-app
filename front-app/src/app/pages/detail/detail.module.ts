import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail-component/detail.component';
import { CardDetailComponent } from './detail-component/card-detail/card-detail.component';

@NgModule({
  declarations: [DetailComponent, CardDetailComponent],
  imports: [
    CommonModule,
    DetailRoutingModule
  ]
})
export class DetailModule { }
