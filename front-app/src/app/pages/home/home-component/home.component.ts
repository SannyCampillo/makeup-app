import { Component, OnInit } from '@angular/core';
import { Title, Main } from './../model/ihome';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
public main: Main = {
  image: 'https://imagesvc.meredithcorp.io/v3/mm/image?q=85&c=sc&poi=face&w=2150&h=1126&url=https%3A%2F%2Fstatic.onecms.io%2Fwp-content%2Fuploads%2Fsites%2F34%2F2019%2F04%2F12171100%2Fvegan-makeup-anna-ok-getty-0419.jpg',
};
public title: Title = {
  title: 'Natural and healthy makeup',
  subtitle: 'WE CARE ABOUT THE PLANET, ANIMALS, PEOPLE AND NATURE.',
};
  constructor() { }

  ngOnInit(): void {
  }

}
