import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HaulListRoutingModule } from './haul-list-routing.module';
import { HaulListComponent } from './haul-list-component/haul-list.component';
import { CardListComponent } from './haul-list-component/card-list/card-list.component';


@NgModule({
  declarations: [HaulListComponent, CardListComponent],
  imports: [
    CommonModule,
    HaulListRoutingModule
  ]
})
export class HaulListModule { }
