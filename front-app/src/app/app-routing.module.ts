import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
    import('./pages/home/home.module').then((m) => m.HomeModule),

  },
  {
    path: 'haul-list',
    loadChildren: () =>
    import('./pages/haul-list/haul-list.module').then((m) => m.HaulListModule),
  },
  {
    path: 'makeupcreate',
    loadChildren: () =>
    import('./pages/create/create.module').then((m) => m.CreateModule),
  },
  {
    path: 'about',
    loadChildren: () =>
    import('./pages/about/about.module').then((m) => m.AboutModule),
  },
  {
    path: 'detail/:id',
    loadChildren: () =>
    import('./pages/detail/detail.module').then((m) => m.DetailModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
