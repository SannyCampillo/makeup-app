import { Component, OnInit, Input } from '@angular/core';
import { MakeupApiList } from 'src/app/models/models-interface';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {
  @Input() card: MakeupApiList;

  constructor() { }

  ngOnInit(): void {
  }

}
