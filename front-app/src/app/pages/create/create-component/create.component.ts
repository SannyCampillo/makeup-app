import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MakeupApiDetail } from 'src/app/models/models-interface';
import { MakeupService } from './../../../services/makeup.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  public makeupForm: FormGroup;
    // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;
    // tslint:disable-next-line: no-inferrable-types
  @Input() public showModal: boolean = false;

  constructor(private formBuilder: FormBuilder, private makeupService: MakeupService) {}

  ngOnInit(): void {
    this.createMakeupForm();
  }

  public createMakeupForm(): void {
    this.makeupForm = this.formBuilder.group({
      id: ['', [Validators.required]],
      brand: ['', [Validators.required]],
      name: ['', [Validators.required]],
      image_link: ['', [Validators.required]],
      description: ['', [Validators.required]],
      product_type: ['', [Validators.required]],
      colorname: ['', [Validators.required]],
      hexvalue: ['', [Validators.required]],
    });
  }
  public onSubmit(): void {
    this.submitted = true;
    this.showModal = true;
    if (this.makeupForm.valid) {
      this.postMakeup();
      this.makeupForm.reset();
      this.submitted = false;
    }
  }

  public postMakeup(): void {
    const makeup: MakeupApiDetail = {
      id: this.makeupForm.get('id').value,
      brand: this.makeupForm.get('brand').value,
      name: this.makeupForm.get('name').value,
      image_link: this.makeupForm.get('image_link').value,
      description: this.makeupForm.get('description').value,
      product_type: this.makeupForm.get('product_type').value,
      product_colors: {
        colorname: this.makeupForm.get('colorname').value,
        hexvalue: this.makeupForm.get('hexvalue').value,
      },
    };
    this.makeupService.postDetail(makeup).subscribe(
      (data: MakeupApiDetail) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );
    this.makeupService.postList(makeup).subscribe(
      (data: MakeupApiDetail) => {
        console.log(data);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}
