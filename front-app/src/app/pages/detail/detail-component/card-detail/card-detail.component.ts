import { Component, OnInit, Input } from '@angular/core';
import { MakeupApiDetail } from 'src/app/models/models-interface';

@Component({
  selector: 'app-card-detail',
  templateUrl: './card-detail.component.html',
  styleUrls: ['./card-detail.component.scss']
})
export class CardDetailComponent implements OnInit {
  @Input() detailcard: MakeupApiDetail;

  constructor() { }

  ngOnInit(): void {
  }

}
