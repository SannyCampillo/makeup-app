import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create-component/create.component';
import { ModalCreateComponent } from './create-component/modal-create/modal-create.component';


@NgModule({
  declarations: [CreateComponent, ModalCreateComponent],
  imports: [
    CommonModule,
    CreateRoutingModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class CreateModule { }
