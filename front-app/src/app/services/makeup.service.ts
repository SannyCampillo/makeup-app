import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { URL} from '../key/urls';
import { MakeupApiList } from '../models/models-interface';
import { MakeupApiDetail } from '../models/models-interface';

@Injectable({
  providedIn: 'root'
})
export class MakeupService {
  private listUrl = URL.urlList;
  private detailUrl = URL.urlDetail;

  constructor(private http: HttpClient) {}


  public getList(): Observable<MakeupApiList[]> {
    return this.http.get(this.listUrl).pipe(
      map((response: MakeupApiList[]) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public getDetail(id: number): Observable<MakeupApiDetail[]> {
    return this.http.get(`${this.detailUrl}/${id}`).pipe(
      map((response: MakeupApiDetail[]) => {
        if (!response) {
          throw new Error('Value expected!');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
    }

    public postDetail(makeupApiDetail: MakeupApiDetail): Observable<any> {
        return this.http.post(this.detailUrl, makeupApiDetail).pipe(
          map((response: any) => {
            if (!response) {
              throw new Error('Value expected!');
            } else {
              return response;
            }
          }),
          catchError((err) => {
            throw new Error(err.message);
          })
        );
    }
    public postList(makeup: MakeupApiDetail): Observable<any> {
      const makeupList: MakeupApiList = {
        id: makeup.id.toString(),
        brand: makeup.brand,
        name: makeup.name,
        image_link: makeup.image_link,
      };
      return this.http.post(this.listUrl, makeupList).pipe(
        map((response: any) => {
          if (!response) {
            throw new Error('Value expected!');
          } else {
            return response;
          }
        }),
        catchError((err) => {
          throw new Error(err.message);
        })
      );
    }
  }
