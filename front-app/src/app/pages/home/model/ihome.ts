export interface Main {
    image: string;
}

export interface Title {
    title: string;
    subtitle: string;
}
