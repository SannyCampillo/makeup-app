import { Component, OnInit, Input } from '@angular/core';

import { Title } from './../../model/ihome';


@Component({
  selector: 'app-home-title',
  templateUrl: './home-title.component.html',
  styleUrls: ['./home-title.component.scss']
})
export class HomeTitleComponent implements OnInit {
  @Input() title: Title;

  constructor() { }

  ngOnInit(): void {
  }

}
